#Setup alias
source $HOME/.alias

#Setup git information
autoload -Uz vcs_info
precmd() { vcs_info }

#Setup format in format name git branch in promt
zstyle ':vcs_info:git:*' formats 'on %F{166}⌥ %b '

#Setup PS1
setopt PROMPT_SUBST
PROMPT="%F{49}%n%f@%f%F{49}%m%f: %F{11}%~%f \$vcs_info_msg_0_%f> "
